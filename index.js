// Siapkan routing express kamu disini...
// jangan lupa listen di port sesuai process.env ya, defaultnya bebas misal 8000

const express = require("express");
const app = express();
const { users } = require("./models");
const cors = require('cors')
const controllers = require("./controllers");
const swaggerUI = require("swagger-ui-express")
const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { password } = require("pg/lib/defaults");


const { PORT = 8000 } = process.env;

app.use(express.json());
app.use(cors());

const options = {
    swaggerOptions: {
        url: "/api-docs"
    }
}

app.use("/docs", cors(), swaggerUI.serve, swaggerUI.setup(null, options))
app.get("/api-docs", cors(), (req, res) => {
    res.sendFile(__dirname + "/swagger.yaml")
})


function authenticateToken(req,res,next) {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(" ")[1];

    if(token === null) return res.sendStatus(401);

    jwt.verify(token, "secretKey", (err, user)=>{
        if(err) return res.sendStatus(403);
        req.user = user;
        next();
    })

}

app.post("/api/v1/login", async (req, res)=>{
    if(req.body.username && req.body.password) {
        const username = req.body.username;
        const password = req.body.password;

        const data = await users.findOne({
            where: {
                username: username
            }
        })

        if(!data) {
            res.json("email atau password salah")
        } else {
            const compare = await bcryptjs.compare(password, data.password)

            if (compare) {
                const accessToken = jwt.sign(data.dataValues, "secretKey");
                res.json(accessToken);
            } else {
                res.json("email atau password salah")
            }
        }
    } else {
        res.json("masukan email/password")
    }
})

//Car Management Endpoints
app.get("/api/v1/cars", controllers.getCars)
app.get("/api/v1/car/:id", controllers.getCar)
app.post("/api/v1/addCar", authenticateToken, controllers.postCar);
app.put("/api/v1/updateCar/:id",authenticateToken, controllers.updateCar)
app.delete("/api/v1/deleteCar/:id",authenticateToken, controllers.deleteCar)


//Registrasi Member
app.post("/api/v1/register", async (req,res) => {
    let data = req.body;
    const {password} = data;

    const hash = await bcryptjs.hash(password, 10);

    data.password = hash;

    data.role = "member";
    
    await users.create(data).then(()=>{
        res.json("data berhasil ditambahkan");
    })
})


app.get("/api/v1/profile", authenticateToken, (req,res)=>{
    const user = req.user;
    res.json(user);
})

//User Management Endpoints
app.post("/api/v1/admin", authenticateToken, async (req,res)=>{
    try {
        const user = req.user;
        if(user.role !== "superadmin") {
            res.status(401).json({message: "tidak memiliki akses"});
        } else {
            const {password} = req.body;
            let data = req.body;
            const hash = await bcryptjs.hash(password, 10);
            data.password = hash;
            await users.create(data)

            res.status(200).json("ok")
        }
    } catch (error) {
        console.log(error);
        res.status(500).json("something went wrong");
    }

})

app.get("/api/v1/admins", authenticateToken, async (req,res)=>{
    try {
        const user = req.user;
        if(user.role !== "superadmin") {
            res.status(401).json({message: "tidak memiliki akses"});
        } else {
            const data = await users.findAll();
        let dataAdmins = [];
    
        data.forEach(element => {
            element.password = null;
            dataAdmins.push(element);
        });
        res.status(200).json(dataAdmins);
        }
    } catch (error) {
        console.log(error);
        res.status(500).json("something went wrong");
    }
})

app.get("/api/v1/admin/:id",authenticateToken , async (req,res)=>{
    try {
        const user = req.user;
        if(user.role !== "superadmin") {
            res.status(401).json({message: "tidak memiliki akses"});
        } else {
        const id = req.params.id
        let user = await users.findOne({
            where: {
                id: id
            }
        })
        user.password = null;
        res.status(200).json(user);
    }
    } catch (error) {
        console.log(error);
        res.status(500).json("something went wrong");
    }
})

app.put("/api/v1/admin/:id",authenticateToken, async (req,res)=>{
    try {
         const user = req.user;
        if(user.role !== "superadmin") {
            res.status(401).json({message: "tidak memiliki akses"});
        } else {
        const id = req.params.id;
        let data = req.body;
        console.log(data);
        
        if(data.password) {
            const hash = await bcryptjs.hash(data.password, 10);
            data.password = hash;
        }

        users.update(data, {
            where: {
                id: id
            }
        })

        res.status(200).json("ok")
    }
    } catch (error) {
        console.log(error);
        res.status(500).json("something went wrong");
    }
})

app.delete("/api/v1/admin/:id", authenticateToken, async (req,res)=>{
    try {
         const user = req.user;
        if(user.role !== "superadmin") {
            res.status(401).json({message: "tidak memiliki akses"});
        } else {
        const id = req.params.id
        users.destroy({
            where: {
                id: id
            }
        })
        res.status(200).json("data berhasil dihapus");
    }
    } catch (error) {
        console.log(error);
        res.status(500).json("something broke!");
    }
})

app.listen(PORT, ()=>{
    console.log(`listen to port ${PORT}`)
})