const { car_rental } = require("../models");

const deleteCar = (req,res)=>{
    const user = req.user;
    if(user.role !== "superadmin" && user.role !== "admin") {
        res.status(401).json({message: "tidak memiliki akses"});
    } else {
        const id = req.params.id;
        const userId = req.body.user;
        car_rental.update({
            is_deleted: true,
            deleted_by: userId
        }, {
            where:
            {
            id: id
            }
        }).then(()=>{
            res.json({message: "data berhasil dihapus"})
        })
    }
}

module.exports = deleteCar;