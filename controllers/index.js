// exports semua controller disini ...
module.exports = {
    postCar: require("./post-car"),
    getCars: require("./get-cars"),
    getCar: require("./get-car"),
    updateCar: require("./update-car"),
    deleteCar: require("./delete-car"),
    
}