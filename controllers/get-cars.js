const { car_rental } = require("../models");

const getCars = async (req, res)=>{
   const cars = await car_rental.findAll()
   let newCars = []
   cars.forEach(car => {
       if (car.dataValues.is_deleted === null) {
           newCars.push(car);
       }
   });
    res.status(201).json(newCars);
}

module.exports = getCars;