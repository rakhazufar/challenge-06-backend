const { car_rental } = require("../models");

const updateCar = async (req,res)=>{
    const user = req.user;
    if(user.role !== "superadmin" && user.role !== "admin") {
        res.status(401).json({message: "tidak memiliki akses"});
    } else {
        const id = req.params.id
        const data = req.body;

        const car = await car_rental.findOne({
            where: {
                id: id
            }
        })

        if(car) {
            if(!car.dataValues.is_deleted) {
                car_rental.update(data, {
                    where: {
                        id: id
                    }
            }).then(()=>{
                res.json({message: "update success"})
            })
            } else {
                res.json("data sudah dihapus")
            }
        } else {
            res.json("data tidak ditemukan")
        }
    }

}
module.exports = updateCar;