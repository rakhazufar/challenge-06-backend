const { car_rental } = require("../models");

const getCar = async (req,res)=>{
    const id = req.params.id
    const car = await car_rental.findOne({
        where: {
            id: id
        }
    });

    if(!car) {
        res.json("data tidak ditemukan")
    } else {
        res.json(car);
    }
}

module.exports = getCar;