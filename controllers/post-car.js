const { car_rental } = require("../models");

const postCar = (req, res)=>{
    const user = req.user;
    if(user.role !== "superadmin" && user.role !== "admin") {
        res.status(401).json({message: "tidak memiliki akses"});
    } else {
        let data = req.body;

        car_rental.create(data).then(
            data => {
                console.log(data)
            }
        )

        res.status(200).send("data berhasil ditambahkan");
    }
}

module.exports = postCar;