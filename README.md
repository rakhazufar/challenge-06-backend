# Car Management Dashboard
Car management dashboard for superadmin, admin, and member;



## How to run



Install all packages:
```bash
yarn install
```

To running running seeder (generate first superadmin):
```bash
npx sequelize-cli db:seed:all
```

To start app:
```bash
yarn migrate
yarn start
```

## Endpoints

### Login and Register

Login Member & admin = '/api/v1/login'

Register (Only for member) = '/api/v1/register'

### Car Management

GET list cars = '/api/v1/cars'  

GET car by id = '/api/v1/car/:id'

POST new car = '/api/v1/addCar'

UPDATE car by id = '/api/v1/updateCar/:id'

DELETE car = '/api/v1/deleteCar/:id'

GET profile = '/api/v1/profile'

### Api Docs

GET swagger docs = '/docs'

GET swagger api = '/api-docs'

### Administrator Management
POST new admin = '/api/v1/admin'

GET list admins = '/api/v1/admins'

GET admin by id = '/api/v1/admin/:id'

UPDATE admin = '/api/v1/admin/:id'

DELETE ADMIN = '/api/v1/admin/:id'

## Directory Structure

```
.
├── config
│   └── config.json
├── controllers
│   ├── delete-car.js
│   ├── get-car.js
│   ├── ger-cars.js
│   ├── index.js
│   ├── post-car.js
│   └── update-car.js
├── migrations
├── models
│   ├── index.js
│   ├── car_rental.js
│   ├── sizes.js
│   └── users.js 
├── seeders
│   └── 20220513152323-demo-user.js
├── .gitignore
├── README.md
├── index.js
├── package.json
├── swagger.yaml
└── yarn.lock
```

## ERD
![Entity Relationship Diagram](public/img/erd.png)
