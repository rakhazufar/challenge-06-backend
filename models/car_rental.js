'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class car_rental extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  car_rental.init({
    name: DataTypes.STRING,
    price: DataTypes.INTEGER,
    size_id: DataTypes.INTEGER,
    image: DataTypes.STRING,
    is_deleted: DataTypes.BOOLEAN,
    created_by: DataTypes.INTEGER,
    updated_by: DataTypes.INTEGER,
    deleted_by: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'car_rental',
    underscored: true,
    freezeTableName: true
  });
  return car_rental;
};