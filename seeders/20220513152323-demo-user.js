'use strict';


const bcryptjs = require("bcryptjs");

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('users', [{
      username: 'binaracademy',
      password: await bcryptjs.hash("rahasiaDong", 10),
      role: "superadmin",
      address: "bogor",
      phone_number: "0896211412421",
      created_at: new Date(),
      updated_at: new Date()
    }]);
  },

  async down (queryInterface, Sequelize) {
   return queryInterface.bulkDelete('users', null, {});
  }
};
